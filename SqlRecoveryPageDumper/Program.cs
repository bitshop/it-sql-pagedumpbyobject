using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

//
// SQL Dumper can be used to see data that was deleted and recreate (restore from backup, don't do this!)..
//     it was originally used for a production db problem where a client didn't have anything but daily backups
//	   and wasn't willing to lose a business day's of updates to the database after a "dba" did a wrong delete
//
// Written by Steve Radich / www.BitShop.com
//
// PROVIDED AS IS - NO WARRANTY OF ANY KIND. You are welcome to fork, use, or do anything with.
// 

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int PageNum;
            Boolean FoundData, FoundUnAlloc;
            int FoundObjID;

            Console.Write("Database Name: ");
            String DBName = Console.ReadLine();
			
            Console.Write("Please enter connection string: ");
            String ConnStr = Console.ReadLine();
            SqlConnection sqlc = new SqlConnection(ConnStr);

            Console.WriteLine("If you know the ObjectID of the missing table then we will filter deleted pages");
            Console.WriteLine("otherwise we will dump object IDs found");
            Console.WriteLine();
            Console.Write("Object ID (Optional):");
            String ObjId = Console.ReadLine();

            Console.Write("Starting Page # (Usually 0): ");
            String StartPage = Console.ReadLine();

            Console.Write("Ending Page # (Usually the last page # of the db): ");
            String EndPage = Console.ReadLine();

            int startpage = 0, endpage = 0, objid = 0;

            try {
                startpage = Convert.ToInt32(StartPage);
                endpage = Convert.ToInt32(EndPage);
            }
            catch {
                Console.WriteLine("Abort: Failed to convert input to string");
                Environment.Exit(-1);
            }

            if (ObjId.Length > 0)
            {
                try
                {
                    objid = Convert.ToInt32(ObjId);
                }
                catch
                {
                    Console.WriteLine("Abort: Failed to convert Object ID");
                    Environment.Exit(-1);
                }
            }

            sqlc.Open();

            for (PageNum = startpage; PageNum < endpage; PageNum++ )
            {
                SqlCommand sqlcmd = new SqlCommand("dbcc traceon(3604); dbcc page(" + dbname + ", 1, @PageNum, 3) WITH TABLERESULTS", sqlc);
                sqlcmd.Parameters.Add("@PageNum", System.Data.SqlDbType.Int);
                sqlcmd.Parameters["@PageNum"].Value = PageNum;
                SqlDataReader sqlrd = sqlcmd.ExecuteReader();
                FoundData = false;
                FoundUnAlloc = false;
                FoundObjID = 0;
                try
                {
                    while (sqlrd.Read())
                    {
                        if (sqlrd[0].ToString() == "PAGE HEADER:" && sqlrd[2].ToString() == "m_objId")
                            FoundObjID = Convert.ToInt32(sqlrd[3]);
                        if (sqlrd[0].ToString() == "PAGE: (1:" + PageNum.ToString() + ")")
                        {
                            if (sqlrd[2].ToString() == "GAM (1:2)" && sqlrd[3].ToString() == "NOT ALLOCATED")
                            {
                                FoundUnAlloc = true;
                                //                        Console.WriteLine("Page [" + PageNum.ToString() + "] - Allocation = [" + sqlrd[3].ToString() + "]");
                            }
                            if (sqlrd[1].ToString().StartsWith("Slot") && FoundUnAlloc)
                            {
                                //                        Console.WriteLine("Page [" + PageNum.ToString() + "] - Found Slot of Data");
                                FoundData = true;
                            }
                        }
                    }
                }
                catch
                {
                    Console.WriteLine("Error on Page (this seems to be fairly normal) " + PageNum.ToString());
                }
                Console.Write("Proccessing Page [" + PageNum.ToString() + "]\r");
                if (FoundData)
                    if ((objid > 0 && FoundObjID == objid) || objid == 0)
                        Console.WriteLine("Page [" + PageNum.ToString() + " - FoundData [" + FoundData.ToString() + "] - FoundUnAlloc [" + FoundUnAlloc.ToString() + "] - ObjID [" + FoundObjID.ToString() + "]");
                sqlrd.Dispose();
                sqlcmd.Dispose();
            }
            sqlc.Close();
        }
    }
}
